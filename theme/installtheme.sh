#! /usr/bin/sh

    # Directories
    USERAPPLYTHEME_SCRIPT_DIR="/usr/bin"
    USERAPPLYTHEME_AUTOSTART_DIR="/home/user/.config/autostart"
    AURORAE_DIR="/usr/share/aurorae/themes"
    SCHEMES_DIR="/usr/share/color-schemes"
    THEME_DIR="/usr/share/plasma/look-and-feel"
    PLASMA_DIR="/usr/share/plasma/desktoptheme"
    GTK_DIR="/usr/share/themes"
    KONSOLE_DIR="/usr/share/konsole"
    CURSORS_DIR="/usr/share/icons"
    WALLPAPERS_DIR="/usr/share/wallpapers"
    SDDM_DIR="/usr/share/sddm/themes"
    XSETTINGSD_DIR="/home/user/.config/xsettingsd"
    SRC_DIR=$(cd $(dirname $0) && pwd)

    THEME_NAME=PlagueOS

install() {

    [[ ! -d ${USERAPPLYTHEME_AUTOSTART_DIR} ]] && mkdir -p ${USERAPPLYTHEME_AUTOSTART_DIR}
    [[ ! -d ${AURORAE_DIR} ]] && mkdir -p ${AURORAE_DIR}
    [[ ! -d ${SCHEMES_DIR} ]] && mkdir -p ${SCHEMES_DIR}
    [[ ! -d ${THEME_DIR} ]] && mkdir -p ${THEME_DIR}
    [[ ! -d ${PLASMA_DIR} ]] && mkdir -p ${PLASMA_DIR}
    [[ ! -d ${GTK_DIR} ]] && mkdir -p ${GTK_DIR}
    [[ ! -d ${KONSOLE_DIR} ]] && mkdir -p ${KONSOLE_DIR}
    [[ ! -d ${CURSORS_DIR} ]] && mkdir -p ${CURSORS_DIR}
    [[ ! -d ${WALLPAPERS_DIR} ]] && mkdir -p ${WALLPAPERS_DIR}
    [[ ! -d ${SDDM_DIR} ]] && mkdir -p ${SDDM_DIR}
    [[ ! -d ${XSETTINGSD_DIR} ]] && mkdir -p ${XSETTINGSD_DIR}

    cp ${SRC_DIR}/user-applytheme.sh                                                   ${USERAPPLYTHEME_SCRIPT_DIR}
    cp -r ${SRC_DIR}/Aurorae/*                                                         ${AURORAE_DIR}
    cp -r ${SRC_DIR}/Colorscheme/*                                                     ${SCHEMES_DIR}
    cp -r ${SRC_DIR}/Lookandfeel/*                                                     ${THEME_DIR}
    cp -r ${SRC_DIR}/Desktoptheme/*                                                    ${PLASMA_DIR}
    cp -r ${SRC_DIR}/gtkTheme/*                                                        ${GTK_DIR}
    cp -r ${SRC_DIR}/Konsole/*                                                         ${KONSOLE_DIR}
    cp -r ${SRC_DIR}/Cursor/*                                                          ${CURSORS_DIR}
    cp -r ${SRC_DIR}/Wallpaper/*                                                       ${WALLPAPERS_DIR}
    cp -r ${SRC_DIR}/Sddm/*                                                            ${SDDM_DIR}
    cp -r ${SRC_DIR}/Xsettingsd/*                                                      ${XSETTINGSD_DIR}

    chmod 555                                                                          ${USERAPPLYTHEME_SCRIPT_DIR}/user-applytheme.sh
    chmod -R 555                                                                       ${AURORAE_DIR}
    chmod -R 555                                                                       ${SCHEMES_DIR}
    chmod -R 555                                                                       ${THEME_DIR}
    chmod -R 555                                                                       ${PLASMA_DIR}
    chmod -R 555                                                                       ${GTK_DIR}
    chmod -R 555                                                                       ${KONSOLE_DIR}
    chmod -R 555                                                                       ${CURSORS_DIR}
    chmod -R 555                                                                       ${WALLPAPERS_DIR}
    chmod -R 555                                                                       ${SDDM_DIR}
    chmod -R 555                                                                       ${XSETTINGSD_DIR}

    #                                                                        |
    # Setting the 'user-applytheme.sh' script to be                          |
    # executed as 'user' (only once) after booting into KDE                  |
    #                                                                       /
    # The script will remove itself after it's done.                       |
    #                                                                      |
    touch ${USERAPPLYTHEME_AUTOSTART_DIR}/user-applytheme.sh.desktop
    echo "[Desktop Entry]
Exec=${USERAPPLYTHEME_SCRIPT_DIR}/user-applytheme.sh
Icon=dialog-scripts
Name=user-applytheme.sh
Path=
Type=Application
X-KDE-AutostartScript=true" > ${USERAPPLYTHEME_AUTOSTART_DIR}/user-applytheme.sh.desktop
    chmod 555 ${USERAPPLYTHEME_AUTOSTART_DIR}/user-applytheme.sh.desktop
    # Add the cronjob for deleting the 'user-applytheme.sh' script after the theme has been applied
    touch /etc/cron.daily/temprm
    echo '#!/bin/bash
rm /usr/bin/user-applytheme.sh
rm /etc/cron.daily/temprm' > /etc/cron.daily/temprm
    chmod 555 /etc/cron.daily/temprm

    # Setting the icon theme
    papirus-folders -C paleorange
    kwriteconfig5 --file /home/user/.config/kdeglobals --group Icons --key Theme "Papirus-Dark"

    # Setting the aurorae (window decoration) theme
    kwriteconfig5 --file /home/user/.config/kwinrc --group 'org.kde.kdecoration2' --key 'theme' '__aurorae__svg__Black'

    # Setting the color-scheme theme
    kwriteconfig5 --file /home/user/.config/kdeglobals --group General --key ColorScheme "Black"

    # Setting the lookandfeel theme
    kwriteconfig5 --file /home/user/.config/kdeglobals --group KDE --key LookAndFeelPackage "Plague-Black"

    # Setting the desktop (plasma panel) theme
    kwriteconfig5  --file /home/user/.config/plasmarc  --group Theme --key name "Black"

    # Setting the gtk theme
    gsettings set org.gnome.desktop.interface gtk-theme PlagueDark
    kwriteconfig5 --file /home/user/.config/gtk-3.0/settings.ini --group Settings --key gtk-theme-name "PlagueDark"

    # Setting the konsole theme
    kwriteconfig5 --file /home/user/.config/konsolerc --group 'Desktop Entry' --key 'DefaultProfile' 'Konsole.profile'
    konsoleprofile colors=Konsole

    # Setting the cursor theme
    kwriteconfig5 --file /home/user/.config/kcminputrc --group Mouse --key cursorTheme "White"
    kwriteconfig5 --file /home/user/.config/gtk-3.0/settings.ini --group Settings --key gtk-cursor-theme-name "White"

    # Setting the lock-screen theme (its wallpaper to be black)
    kwriteconfig5 --file /home/user/.config/kscreenlockerrc --group 'Greeter' --key 'WallpaperPlugin' 'org.kde.color'
    kwriteconfig5 --file /home/user/.config/kscreenlockerrc --group 'Greeter' --group 'Wallpaper' --group 'org.kde.color' --group 'General' --key 'Color' '0,0,0'
    # Setting "Lock screen automatically after" to 10 minutes instead of the default 15 minutes
    kwriteconfig5 --file /home/user/.config/kscreenlockerrc --group Daemon --key Timeout "10"
    kwriteconfig5 --file /home/user/.config/kscreenlockerrc --group Daemon --key LockGrace "7"

    # Disabling the splash screen
    kwriteconfig5 --file /home/user/.config/ksplashrc --group KSplash --key Engine "none"
    kwriteconfig5 --file /home/user/.config/ksplashrc --group KSplash --key Theme "none"

    # Setting the task switcher theme
    kwriteconfig5 --file /home/user/.config/kwinrc --group TabBox --key LayoutName "thumbnail_grid"
    kwriteconfig5 --file /home/user/.config/kwinrc --group TabBoxAlternative --key LayoutName "thumbnail_grid"

    # Setting the font theme
    kwriteconfig5 --file /home/user/.config/kdeglobals --group General --key fixed "Hack,10,-1,7,50,0,0,0,0,0"
    kwriteconfig5 --file /home/user/.config/kdeglobals --group General --key font "Open Sans,10,-1,7,50,0,0,0,0,0"
    kwriteconfig5 --file /home/user/.config/kdeglobals --group General --key menuFont "Open Sans,10,-1,7,50,0,0,0,0,0"
    kwriteconfig5 --file /home/user/.config/kdeglobals --group General --key shadeSortColumn "true"
    kwriteconfig5 --file /home/user/.config/kdeglobals --group General --key smallestReadableFont "Open Sans,8,-1,7,50,0,0,0,0,0"
    kwriteconfig5 --file /home/user/.config/kdeglobals --group General --key toolBarFont "Open Sans,10,-1,7,50,0,0,0,0,0"
    kwriteconfig5 --file /home/user/.config/kdeglobals --group General --key desktopFont "Open Sans,10,-1,7,50,0,0,0,0,0"
    kwriteconfig5 --file /home/user/.config/kdeglobals --group General --key taskbarFont "Open Sans,10,-1,7,50,0,0,0,0,0"
    kwriteconfig5 --file /home/user/.config/kdeglobals --group General --key activeFont "Open Sans,10,-1,7,50,0,0,0,0,0"
    kwriteconfig5 --file /home/user/.config/kdeglobals --group WM --key activeFont "Open Sans,10,-1,7,50,0,0,0,0,0"
    kwriteconfig5 --file /home/user/.config/plasma-desktop-appletsrc --group General --key fontTime "Open Sans,10,-1,7,50,0,0,0,0,0"

    # Setting the sddm theme
    kwriteconfig5 --file /etc/sddm.conf.d/kde_settings.conf --group General --key DisplayServer "wayland"
    kwriteconfig5 --file /etc/sddm.conf.d/kde_settings.conf --group Theme --key Current "Plague"
    kwriteconfig5 --file /etc/sddm.conf.d/kde_settings.conf --group Theme --key CursorTheme "White"
    kwriteconfig5 --file /etc/sddm.conf.d/kde_settings.conf --group Theme --key Font "Open Sans,10"

    # Setting the kate (text editor) theme
    kwriteconfig5 --file /home/user/.config/katerc --group 'KTextEditor Renderer' --key 'Auto Color Theme Selection' 'false'
    kwriteconfig5 --file /home/user/.config/katerc --group 'KTextEditor Renderer' --key 'Color Theme' 'ayu Dark'
    kwriteconfig5 --file /home/user/.config/katerc --group 'KTextEditor Renderer' --key 'Font' 'Hack,10,-1,7,50,0,0,0,0,0'
    # Setting the Kate text editor to show the full path in titlebar
    kwriteconfig5 --file /home/user/.config/katerc --group General --key 'Show Full Path in Title' 'true'

    # General KDE settings configuration:   |
    #                                       |
    #                                       |
    # Disabling the sliding popup animation
    kwriteconfig5 --file /home/user/.config/kwinrc --group Plugins --key slidingpopupsEnabled --type bool "false"
    # Enabling cursor animation for application loading
    kwriteconfig5 --file /home/user/.config/klaunchrc --group BusyCursorSettings --key Timeout "3"
    kwriteconfig5 --file /home/user/.config/klaunchrc --group TaskbarButtonSettings --key Timeout "3"
    # Changing notifications timeout duration
    kwriteconfig5 --file /home/user/.config/plasmanotifyrc --group Notifications --key PopupTimeout "4000"
    # Settings for krunner (kde search)
    kwriteconfig5 --file /home/user/.config/krunnerrc --group General --key ActivityAware "false"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group General --key HistoryEnabled "false"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group General --key RetainPriorSearch "false"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group Plugins --key CharacterRunnerEnabled "false"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group Plugins --key DictionaryEnabled "false"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group 'Plugins' --key 'Spell CheckerEnabled' 'false'
    kwriteconfig5 --file /home/user/.config/krunnerrc --group Plugins --key baloosearchEnabled "false"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group Plugins --key bookmarksEnabled "false"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group Plugins --key browsertabsEnabled "false"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group Plugins --key desktopsessionsEnabled "false"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group Plugins --key locationsEnabled "false"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group Plugins --key recentdocumentsEnabled "false"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group Plugins --key servicesEnabled "true"
    kwriteconfig5 --file /home/user/.config/krunnerrc --group Plugins --key webshortcutsEnabled "false"
    # Disabling file indexing
    kwriteconfig5 --file /home/user/.config/baloofilerc --group 'Basic Settings' --key 'Indexing-Enabled' 'false'
    kwriteconfig5 --file /home/user/.config/kcmshell5rc --group 'Basic Settings' --key 'Indexing-Enabled' 'false'
    # Settings for KFileDialog
    kwriteconfig5 --file /home/user/.config/kdeglobals --group 'KFileDialog Settings' --key 'Show hidden files' --type bool 'true'
    kwriteconfig5 --file /home/user/.config/kdeglobals --group 'KFileDialog Settings' --key 'Show Full Path' --type bool 'true'
    # Disabling the recent document tracking
    kwriteconfig5 --file /home/user/.config/kdeglobals --group RecentDocuments --key UseRecent "false"
    # Clean clipboard (klipper) content on exit, and make it not ask for confirmation when cleaning
    kwriteconfig5 --file /home/user/.config/klipperrc --group General --key KeepClipboardContents "false"
    kwriteconfig5 --file /home/user/.config/plasmashellrc --group 'Notification Messages' --key 'klipperClearHistoryAskAgain' 'false'
    # Disabling restoring of the desktop session
    kwriteconfig5 --file /home/user/.config/ksmserverrc --group General --key loginMode "default"
    # Disabling background services
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-baloosearchmodule --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-bluedevil --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-browserintegrationreminder --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-colorcorrectlocationupdater --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-device_automounter --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-freespacenotifier --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-gtkconfig --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-kded_accounts --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-kded_bolt --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-ktimezoned --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-kwrited --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-networkstatus --key autoload "false"
    kwriteconfig5 --file /home/user/.config/kded5rc --group Module-proxyscout --key autoload "false"
    # Disabling the cookie jar
    kwriteconfig5 --file /home/user/.config/kded5rc --group 'Cookie Policy' --key 'AcceptSessionCookies' 'false'
    kwriteconfig5 --file /home/user/.config/kded5rc --group 'Cookie Policy' --key 'CookieGlobalAdvice' 'Reject'
    kwriteconfig5 --file /home/user/.config/kded5rc --group 'Cookie Policy' --key 'Cookies' 'false'
    kwriteconfig5 --file /home/user/.config/kded5rc --group 'Cookie Policy' --key 'RejectCrossDomainCookies' 'false'
    # Disabling web shortcuts
    kwriteconfig5 --file /home/user/.config/kuriikwsfilterrc --group General --key EnableWebShortcuts "false"

}

    echo "Installing the '${THEME_NAME} KDE theme'..."

    install "${THEME_NAME}"

    echo ""
    echo "Installation has finished."
