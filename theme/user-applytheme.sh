#! /usr/bin/sh

    #                                                                        |
    # Setting the 'user-applytheme.sh' script to be                          |
    # executed as 'user' (only once) after booting into KDE                  |
    #                                                                       /
    # The script will remove itself after it's done.                       |
    #                                                                       \
    # All of the theme contents are still available under 'themes' folder.  |
    # All of the temporary files are deleted after the theme is applied.    |
    #                                                                       |

    # Directory
    USERAPPLYTHEME_AUTOSTART_DIR="/home/user/.config/autostart"
    SRC_DIR=$(cd $(dirname $0))

apply() {

    # Setting the lookandfeel theme
    plasma-apply-lookandfeel -a 'Plague-Black'

    # Setting the colorscheme theme
    plasma-apply-colorscheme Black

    # Setting the cursor theme
    plasma-apply-cursortheme White
    xsetroot -cursor_name left_ptr

    # Setting the wallpaper
    qdbus org.kde.plasmashell /PlasmaShell org.kde.PlasmaShell.evaluateScript "var allDesktops = desktops();print (allDesktops);for (i=0;i<allDesktops.length;i++) {d = allDesktops[i];d.wallpaperPlugin = 'org.kde.image';d.currentConfigGroup = Array('Wallpaper', 'org.kde.image', 'General');d.writeConfig('Image', 'file:///usr/share/wallpapers/Plague/contents/images/1920x1080.jpg')}"

    # Setting the Konsole theme
    konsoleprofile colors=Konsole

    # Setting the Yakuake (kde dropdown terminal) theme < - It's not installed by default.
    # qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.runCommand 'konsoleprofile colors=Konsole'

    # Restarting kwin and plasmashell in order to instantly apply the theme on boot
    qdbus org.kde.KWin /KWin reconfigure
    kquitapp5 plasmashell && kstart5 plasmashell

}

    apply "${THEME_NAME}"

    # Removing a temporary file
    rm -f ${USERAPPLYTHEME_AUTOSTART_DIR}/user-applytheme.sh.desktop
